ivyRepoDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
destDir="$(pwd)"
archive="$( date +ii-+%Y%M%d%H%m%s.zip )"

cd $ivyRepoDir
git archive master -o ${destDir}/${archive}
cd $destDir
unzip ${archive}
rm -f ${archive}

