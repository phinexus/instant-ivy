### What is this repository for? ###
>This repo contains a folder called ivy that you can drop into any ant-based project to quickly enable ivy, apache's dependency manager. 

### How do I get set up? ###
>1.  Place the ivy folder in the root of your project.
>2.  In your ant build.xml file, paste:  <import file="ivy/ivy-build.xml"/>
> 
>
> That's it!  You don't even need to set up ivy -- it will be installed automatically for you, along with ant-contrib.
> 
>  **(optional for publication purposes):**
>  Change the line in ivy/ivy.xml:  <info organisation="your.org.name" module="product_name"/>
### How do I use it? ###
> 
> * Enter your project's dependencies in ivy/ivy.xml.
> * Add ${ivy.jars.path} to your classpath (see below).
>
>     **NOTE:**  If you have a /lib directory, ivy will look here first for your project's dependencies.  For example, if you have a dependency on lombok 1.14.2, it will first look for lib/lombok-1.14.2.jar, then for lib/lombok.jar, then download it from the maven repository. 
>
### What do I get? ###
> Oooo... lots of good stuff:
> #### Ant Tasks ####
> * All tasks from ant-contrib will be included.
>
> #### Ant Properties ####
> * **${ivy.jars.path}:** This ant property will be set to contain all ivy-managed jars (in <root>/ivy/lib). Include this in your javac classpath.  
>
>     TIP for Netbeans users:  Reset the javac.classpath property with ant-contrib's var task:  
>
```
#!xml
    <target name="-post-init">
	    <var name="javac.classpath" value="${javac.classpath}${path.separator}${ivy.jars.path}"/>
    </target>
```
>
> 
> #### Ant Targets ####
> The following targets will have been added to your build.xml (in rough order of importance):
> 
> * **ivy-get-jars**:  this target retrieves the dependencies into the ivy/lib directory and keeps it up to date, removing old versions or irrelevant files.
> * **ivy-clean**:  this target deletes the ivy/lib directory and removes the dependencies from the local cache.
> * **ivy-update-cache**:  this target resolves the dependencies into the local cache.